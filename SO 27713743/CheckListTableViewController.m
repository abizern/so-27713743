//
//  CheckListTableViewController.m
//  SO 27713743
//
//  Created by Abizer Nasir on 30/12/2014.
//  Copyright (c) 2014 Jungle Candy Software Ltd. All rights reserved.
//

#import "CheckListTableViewController.h"
#import "CheckListItem.h"


@interface CheckListTableViewController ()

@property (nonatomic, copy) NSArray *items;

@end

@implementation CheckListTableViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupDataSource];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * const reuseIdentifier = @"CheckListItemCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell withCheckListItem:self.items[indexPath.row]];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CheckListItem *item = self.items[indexPath.row];
    item.isChecked = !item.isChecked;
    
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - Private methods

- (void)setupDataSource {
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:5];
    
    CheckListItem *item;
    
    item = [[CheckListItem alloc] init];
    item.text = @"Write code";
    item.isChecked = NO;
    
    [array addObject:item];
    
    item = [[CheckListItem alloc] init];
    item.text = @"Walk the dog";
    item.isChecked = NO;
    
    [array addObject:item];
    
    
    item = [[CheckListItem alloc] init];
    item.text = @"Get Shopping";
    item.isChecked = YES;
    
    [array addObject:item];
    
    item = [[CheckListItem alloc] init];
    item.text = @"Book holiday";
    item.isChecked = NO;
    
    [array addObject:item];
    
    item = [[CheckListItem alloc] init];
    item.text = @"Mow the lawn";
    item.isChecked = NO;
    
    [array addObject:item];
    
    _items = [array copy];
}

- (void)configureCell:(UITableViewCell *)cell withCheckListItem:(CheckListItem *)item {
    cell.textLabel.text = item.text;
    
    if (item.isChecked) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

@end
