//
//  CheckListItem.h
//  SO 27713743
//
//  Created by Abizer Nasir on 30/12/2014.
//  Copyright (c) 2014 Jungle Candy Software Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckListItem : NSObject

@property (nonatomic, copy) NSString *text;
@property (nonatomic) BOOL isChecked;

@end
