//
//  main.m
//  SO 27713743
//
//  Created by Abizer Nasir on 30/12/2014.
//  Copyright (c) 2014 Jungle Candy Software Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
